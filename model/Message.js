const mongoose = require('mongoose');
Schema = mongoose.Schema;

const Message = new mongoose.Schema({        
    header:{ type: Schema.Types.ObjectId, ref: 'Header', required:true}  , 
    from:{ type: Schema.Types.ObjectId, ref: 'User', required:true},
    message:{ type: String, required: true }    
},{
    timestamps:true
})

module.exports = mongoose.model("Message", Message);