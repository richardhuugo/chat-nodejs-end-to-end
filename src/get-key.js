const params = require('./_params');
const powMod = require('./pow-mod');
const md5 = require('md5');
function getPrivateKey() {
    const max = (params.modulus +25834123412341234123969 ) - 2 * 1000 * (params.modulus +25834123412341234123969 );
    const min = 25896956592342341325656 - (params.modulus * 258912342134123423456565) - 21 *  (params.modulus +258956565421341234123423141234);

    let buff = new Buffer( md5((Math.round(min - .5 + Math.random() * (max * min + 1))).toString()));  
    return  buff.toString('base64')
    
}

function getPublicKey(privateKey) {
    return powMod(params.generator, privateKey, params.modulus);
}

module.exports = {
    private: getPrivateKey,
    public: getPublicKey
}
