const mongoose = require('mongoose');
Schema = mongoose.Schema;

const TokenHeader = new mongoose.Schema({        

 
    header:{ type: Schema.Types.ObjectId, ref: 'Header' },            
    user:{ type: Schema.Types.ObjectId, ref: 'User' },       
    token:{type:String, required:false}
     
    

},{
    timestamps:true
})

module.exports = mongoose.model("TokenHeader", TokenHeader);
