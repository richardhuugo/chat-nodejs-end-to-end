global.hashs =[];
const express = require('express');
const bodyParser = require('body-parser');
const queryParser = require('express-query-int');
const app = express();
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())      
app.use(queryParser())
app.use(express.static(__dirname + "/public"));
const axios = require('axios');
const port = 3000;
axios.defaults.baseURL = 'http://localhost:' + port;
require('./src/database');
var server = require('http').Server(app);
var io = require('socket.io')(server)

const auth = require('./src/auth');
const {login, create} = require('./src/routes/routes')
require('./src/socket/middleware')(io);

app.get('/', function(req, res){
    res.redirect('login.html')
});

app.get('/room', function(req, res){
    res.redirect('room.html')
});

 
app.get('/login', function(req, res){
    res.redirect('login.html')
});
 

const protectedApi = express.Router()
app.use('/close', protectedApi)
protectedApi.use(auth)
   

app.post('/login-user', login)
app.post('/create-user',create);
 



server.listen(port, '0.0.0.0', function() {
    console.log('listening ' + port);
})


module.exports ={app, server, io}
 
 