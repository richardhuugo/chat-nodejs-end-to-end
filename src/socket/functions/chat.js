const User = require('../../../model/User');
const Message = require('../../../model/Message');
const Header = require('../../../model/Header');
const sha1 = require('sha1');
const md5 = require('md5');
const powMod = require('../../pow-mod');
const params = require('../../_params');
const jwt = require('jsonwebtoken');
const CryptoJS = require('crypto-js')
const AES = CryptoJS.AES;
const enc = CryptoJS.enc;
var moment = require('moment-timezone');
moment().tz("America/Sao_Paulo").format();
const {createUserTokenHeader,criaHeader,verificarTokenHeader, cripMessage} = require('./functions');


module.exports = (socket ,io ) => {
    socket.on('message', function(message) {
        io.emit('message', message);
    });

    socket.on('searchContacts',(contato) => {
        const me    = socket.decoded;
        User.find({nome:contato},(error, result) => {
            io.emit('contatos'+me.user._id, result);
         })         
    })

   socket.on('get-message', async (friend) => {
     const me    = socket.decoded;
   
    const fromHash =  hashs.filter((object) => {return object["id"] ==me.user._id})    
    let headers = await Header.findOne({users:{ "$all" : [friend, me.user._id]}});
    if(!headers){
         headers = await Header.findOne({users:{ "$all" : [me.user._id, friend]}});
    }  
    if(headers ){        
   
        const createTokenheader = await verificarTokenHeader( me.user._id, headers);
        const tokenDecoded = jwt.verify(createTokenheader.token,sha1( fromHash[0].hash ) );
        
        const messagem = await Message.find({header:headers._id}) 
        let resp = await  messagem.map((message,index) => {
             
            let text = AES.decrypt(message.message,  tokenDecoded.secret.toString()).toString(enc.Utf8);       
            let buff = new Buffer(text, 'base64');  
            let createdAt = moment.utc(message.createdAt).local().format();
            let updatedAt = moment.utc(message.updatedAt).local().format();
            let object = {from: message.from, message:buff.toString('utf8') , createdAt, updatedAt}
            return object
           
        })
        io.emit('read-message'+me.user._id+''+friend, resp); 
      
    }
    
      
    
      
   })  

   socket.on('get-header', async () => {
        const me    = socket.decoded;
        Header.find({users:{ "$all" : [me.user._id]}}).populate('users').exec((error, header) => {
            let number =0
            if(header){
                io.emit('headers'+me.user._id, {header, number}); 
            }else{
                Header.find({users:{ "$all" : [me.user._id]}}).populate('users').exec((error, header2) => {
                    io.emit('headers'+me.user._id, {header:header2, number}); 
                })
            }
            
            
        })
   })
    
    socket.on('send-message',async ({to, message}) => {
        let from = socket.decoded.user._id
        const me    = socket.decoded;
    
        const users =   [to,from];
        let secretMe =0
        let trueLine =true;
        let verify =[to, me.user._id]
        let headers = await Header.findOne({users:{ "$all" : [to,from]}});     
        if(!headers){
            headers = await Header.findOne({users:{ "$all" : [from, to]}});     
        }
        
        let headerCreated =""
             
        let fromHash =  hashs.filter((object) => {return object["id"] ==from})
        let toHash =hashs.filter((object) => {return object["id"] ==to})
        if(!headers){
             headerCreated = await criaHeader(users,null )             
             secretMe = powMod( me.public, me.private ,params.modulus)                   
             let data = await createUserTokenHeader( toHash[0].hash, secretMe,  to,headerCreated )
             let data2 = await createUserTokenHeader(fromHash[0].hash, secretMe, from,  headerCreated )
             if(data && data2){
               
               let tokenDecoded = jwt.verify(data2.token,sha1( fromHash[0].hash ) );
               encryptMe = AES.encrypt(message, tokenDecoded.secret.toString());
               new Message({
                   header:headerCreated._id,
                   from ,
                   message:encryptMe     
               }).save(async (error, messages) => {
                   const messagem =  await Message.find({header:headerCreated._id}) 
                   let resp = await   messagem.map((response,index) => {
                
                       let text = AES.decrypt(response.message,  tokenDecoded.secret.toString()).toString(enc.Utf8);       
                       let buff = new Buffer(text, 'base64');  
                       let createdAt = moment.utc(message.createdAt).local().format();
                       let updatedAt = moment.utc(message.updatedAt).local().format();
                       let object = {from: response.from, message:buff.toString('utf8') , createdAt, updatedAt}
                       return object
                      
                   })
       
           //     if(toHash.length > 0){
                       Header.findOne({users:{ "$all" :  [to,from]  }}).populate('users').exec((error, header) => {
                           let number = 1
                           if(header){
                                io.emit('headers'+me.user._id,{header,number}); 
                                io.emit('headers'+to,  {header,number}); 
                                io.emit('read-message'+from+''+to, resp);
                                io.emit('read-message'+to+''+from, resp);  
                           }else{
                            Header.findOne({users:{ "$all" :  [from, to]  }}).populate('users').exec((error, header2) => {
                                io.emit('headers'+me.user._id,{header:header2,number}); 
                                io.emit('headers'+to,  {header:header2,number}); 
                                io.emit('read-message'+from+''+to, resp);
                                io.emit('read-message'+to+''+from, resp);  
                            })
                           }
                             
                       })
                    
       /**
                   }else{
                       Header.findOne({users:{ "$all" :  [to,from]  }}).populate('users').exec((error, header) => {
                           let number = 1
                           if(header){
                            io.emit('headers'+me.user._id,{header,number}); 
                            io.emit('read-message'+from+''+to, resp);  
                            }else{
                                Header.findOne({users:{ "$all" :  [from, to]  }}).populate('users').exec((error, header2) => {                                    
                                    io.emit('headers'+me.user._id,{header:header2,number}); 
                                    io.emit('read-message'+from+''+to, resp);
                                })
                            }
                          
                       })
                   }    */   
                   
               }) 
               
           }
        } else {
            console.log({from, headers})
            let createTokenheader = await verificarTokenHeader( from, headers);
            console.log(createTokenheader)
            let tokenDecoded = jwt.verify(createTokenheader.token,sha1( fromHash[0].hash ) );
      
            encryptMe = AES.encrypt(message, tokenDecoded.secret.toString());
            
              new Message({
                header:headers._id,
                from ,
                message:encryptMe     
            }).save(async (error, messages) => {
                const messagem =  await Message.find({header:headers._id}) 
                let resp = await   messagem.map((response,index) => {
             
                    let text = AES.decrypt(response.message,  tokenDecoded.secret.toString()).toString(enc.Utf8);       
                    let buff = new Buffer(text, 'base64');  
                    let createdAt = moment.utc(message.createdAt).local().format();
                    let updatedAt = moment.utc(message.updatedAt).local().format();
                    let object = {from: response.from, message:buff.toString('utf8') , createdAt, updatedAt}
                    return object
                   
                })                
              //  if(toHash.length > 0){
                    Header.findOne({users:{ "$all" :  [to,from]  }}).populate('users').exec((error, header) => {
                        let number = 1
                        if(header){
                            console.log('headers'+to)
                             io.emit('headers'+me.user._id,{header,number}); 
                             io.emit('headers'+to,  {header,number}); 
                             io.emit('read-message'+from+''+to, resp);
                             io.emit('read-message'+to+''+from, resp);  
                        }else{
                         Header.findOne({users:{ "$all" :  [from, to]  }}).populate('users').exec((error, header2) => {
                             io.emit('headers'+me.user._id,{header:header2,number}); 
                             io.emit('headers'+to,  {header:header2,number}); 
                             console.log('headers'+to)
                             io.emit('read-message'+from+''+to, resp);
                             io.emit('read-message'+to+''+from, resp);  
                         })
                        }
                          
                    })
                     
             /**   }else{
                    Header.findOne({users:{ "$all" :  [to,from]  }}).populate('users').exec((error, header) => {
                        let number = 1
                        if(header){
                         io.emit('headers'+me.user._id,{header,number}); 
                         io.emit('read-message'+from+''+to, resp);  
                         }else{
                             Header.findOne({users:{ "$all" :  [from, to]  }}).populate('users').exec((error, header2) => {                                    
                                 io.emit('headers'+me.user._id,{header:header2,number}); 
                                 io.emit('read-message'+from+''+to, resp);
                             })
                         }
                       
                    })                     
                }   */  
                
            }) 
        }
         
       
    })

}