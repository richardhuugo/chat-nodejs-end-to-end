const User = require('../../model/User');
const jwt = require('jsonwebtoken');
const sha1 = require('sha1');
const md5 = require('md5');
const Participant = require('../participant');
exports.login = (req, res, next) => {
    const email = req.body.email ||''
    const senha = req.body.password || ''

    User.findOne({email , senha:sha1(md5(senha))}, (error, user) =>{
        if(error){
            return res.status(400).send({message:'error '+error, status:false})
        }

        if(!user){
            return res.status(400).send({message:'Usuario ou senha invalidos',status:false, user})
        }else{
            jwt.verify(user.token,sha1( md5(senha)), function (err, decoded) {
                if (err) {
                    return res.status(400).send({message:'error '+err, status:false})
                } else {
                    let date = {
                        id:user._id,
                        hash:(sha1(senha+''+user._id))
                    }
                    global.hashs.push(date)
                   
                    const token = jwt.sign({
                        decoded,
                        user
                    } , sha1(senha+''+user._id), {
                        expiresIn: "18250d"
                    })

                    return res.status(200).send({token,id:user._id, status:true})
                    

                }
            })
        }
    })
}
exports.create = (req, res, next) => {
    const nome = req.body.nome ||''
    const sobrenome = req.body.sobrenome ||''
    const senha = req.body.senha ||''
    const email = req.body.email ||''

    
     User.findOne({email}, (error, user) => {
         if(error){
            return res.status(400).send({message:'Não conseguimos concluir a transacao'})
         }

         if(!user){
            
            sender = new Participant( email)  ;
             
            
           const data = {
                private:sender.keyPair.private,
                public:sender.keyPair.public,
                hash:sha1(sender.keyPair.public+''+md5( senha))

            }
            const token = jwt.sign(data , sha1(md5( senha)), {
                expiresIn: "18250d"
            })
            
            const userPass = sha1(md5(senha))

            new User({
                nome,
                sobrenome,
                senha:userPass,
                email,
                publicKey:sender.keyPair.public,
                token 
            }).save((error, sucesso) => {
                if(error){
                    return res.status(400).send({message:'Não conseguimos registrar o usuario, tente novamente'})
                }
                return res.status(200).send({message:'Cadastro realizado com sucesso'})
            })  
            
         }else{
            return res.status(400).send({message:'Não conseguimos registrar o usuario, usuario existente'})
         }
     })
} 