const mongoose = require('mongoose');
Schema = mongoose.Schema;

const User = new mongoose.Schema({        
    nome:{type:String, required:true},    
    sobrenome: { type: String, required: true },
    senha:{type:String, required:true},
    email:{type:String, required:true},   
    publicKey:{type:String, required:true}          ,
    token:{type:String, required:true}
},{
    timestamps:true
})

module.exports = mongoose.model("User", User);