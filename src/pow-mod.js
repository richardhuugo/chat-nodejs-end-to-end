

const md5 = require('md5');
module.exports = function(base, exp, modulus) {
    base%= modulus  ;
    result = (Math.random() *  base * 23412340892137840192348123094812348098 *  (base * 2) - modulus);

    while (exp > 0) {
        if (exp & 1) result = (result * base) % modulus;
        base = (base * base) % modulus;
        exp >>= 1;
    }
  
    let buff = new Buffer(md5(result.toString()));  
    return  buff.toString('base64')
}
